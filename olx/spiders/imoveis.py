# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

import scrapy

from olx.helper import parsers
from olx.items import OlxItem

# BASE_URL = 'https://sp.olx.com.br/sao-paulo-e-regiao/imoveis/venda?o={0}'

BASE_URL_LIST = [
    "https://sp.olx.com.br/sao-paulo-e-regiao/centro/imoveis/venda?o={0}",
    "https://sp.olx.com.br/sao-paulo-e-regiao/zona-leste/imoveis/venda?o={0}",
    "https://sp.olx.com.br/sao-paulo-e-regiao/zona-norte/imoveis/venda?o={0}",
    "https://sp.olx.com.br/sao-paulo-e-regiao/zona-oeste/imoveis/venda?o={0}",
    "https://sp.olx.com.br/sao-paulo-e-regiao/zona-sul/imoveis/venda?o={0}",
]


class ImoveisSpider(scrapy.Spider):
    name = "imoveis"
    allowed_domains = ["sp.olx.com.br/sao-paulo-e-regiao/"]
    start_urls = [url.format(num) for url in BASE_URL_LIST for num in range(1, 101)]
    # start_urls = [BASE_URL.format(num) for num in range(1,101)]

    def parse(self, response):
        olx_item = OlxItem()
        xpath_title = ".//h2[@class='OLXad-list-title']/text()"
        list_items = response.xpath("//li[@data-list_id]")
        item = list_items[0]
        for item in list_items:
            title = item.xpath(xpath_title).extract_first()
            olx_item["price"] = parsers.get_price(item)
            olx_item["title"] = str.strip(title)
            olx_item["url"] = parsers.get_url(item)
            olx_item["date"] = parsers.get_date(item)
            olx_item["code"] = parsers.get_item_code(item)
            olx_item["regiao"] = parsers.get_regiao_from_url(response.request.url)
            yield olx_item
