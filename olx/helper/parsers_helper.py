from decimal import Decimal


def parse_months_numbers(month):
    dict_parse = {
        "Jan": "01",
        "Feb": "02",
        "Fev": "02",
        "Mar": "03",
        "Apr": "04",
        "Abr": "04",
        "May": "05",
        "Mai": "05",
        "Jun": "06",
        "Jul": "07",
        "Aug": "08",
        "Ago": "08",
        "Sep": "09",
        "Set": "09",
        "Out": "10",
        "Oct": "10",
        "Nov": "11",
        "Dec": "12",
        "Dez": "12",
    }
    return dict_parse[month]


def converte_dinheiro_decimal(valor: str) -> Decimal:
    valor = valor.strip("R$ ")
    valor = valor.replace(".", "")
    return Decimal(valor)


def converte_dinheiro_float(valor: str) -> float:
    valor = valor.strip("R$ ")
    valor = valor.replace(".", "")
    return float(valor)
