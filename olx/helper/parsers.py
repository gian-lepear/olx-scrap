from datetime import datetime, timedelta
from olx.helper import parsers_helper


def get_url(item):
    xpath_url1 = './/a[@class="OLXad-list-link "]/@href'
    xpath_url2 = './/a[@class="OLXad-list-link"]/@href'
    xpath_url3 = './/a[@class="OLXad-list-link-featured "]/@href'
    xpath_url4 = './/a[@class="OLXad-list-link-featured"]/@href'
    url1 = item.xpath(xpath_url1).extract_first()
    url2 = item.xpath(xpath_url2).extract_first()
    url3 = item.xpath(xpath_url3).extract_first()
    url4 = item.xpath(xpath_url4).extract_first()
    if url1 is not None:
        return str.strip(url1)
    elif url2 is not None:
        return str.strip(url2)
    elif url3 is not None:
        return str.strip(url3)
    else:
        return str.strip(url4)


def get_price(item):
    xpath_price = './/p[@class="OLXad-list-price"]/text()'
    price = item.xpath(xpath_price).extract_first()
    price = str.strip(price) if price else "0"
    # price = parsers_helper.converte_dinheiro_decimal(price)
    price = parsers_helper.converte_dinheiro_float(price)
    return price


def next_page_url(response):
    next_page_xpath = '//a[@rel="next"]/@href'
    next_page_url = response.xpath(next_page_xpath).extract_first()
    return next_page_url


def get_date(item):
    xpath_date = './/a/div[@class="col-4"]/p/text()'
    date_list = item.xpath(xpath_date).extract()
    date = date_list[0]
    date = _parse_date_str(date)
    time = date_list[1]
    date_time = "{0} {1}".format(date, time)
    date_time = datetime.strptime(date_time, "%d/%m/%Y %H:%M")
    date_time_str = datetime.strftime(date_time, "%d/%m/%Y %H:%M")
    return date_time_str


def get_item_code(item):
    xpath_code = "./@data-list_id"
    code = item.xpath(xpath_code).extract_first()
    return int(code)


def _parse_date_str(date):
    try:
        today = datetime.today()
        if date == "Hoje":
            return today.strftime("%d/%m/%Y")
        elif date == "Ontem":
            yesterday = today - timedelta(days=1)
            return yesterday.strftime("%d/%m/%Y")
        else:
            year = datetime.today().year
            day, month = date.split(" ")
            month = parsers_helper.parse_months_numbers(month)
            return "{0}/{1}/{2}".format(day, month, year)
    except:
        print(date)


def get_regiao_from_url(url):
    base_replace = "https://sp.olx.com.br/sao-paulo-e-regiao/"
    url = url.replace(base_replace, "")
    return url.split("/")[0]

