# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


from sqlalchemy.orm import sessionmaker
from olx.database.models import Imoveis, db_connect, create_imoveis_table
from sqlalchemy.exc import IntegrityError


class OlxPipeline(object):
    def process_item(self, item, spider):
        return item


class ImoveisPipeline(object):
    """Livingsocial pipeline for storing scraped items in the database"""

    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates deals table.
        """
        engine = db_connect()
        create_imoveis_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """Save imoveis in the database.

        This method is called for every item pipeline component.

        """
        session = self.Session()
        imoveis = Imoveis(**item)

        try:
            session.add(imoveis)
            session.commit()
        except IntegrityError:
            pass
        except:
            session.rollback()
            raise
        finally:
            session.close()

        return item
