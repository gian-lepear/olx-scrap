from scrapy.utils.project import get_project_settings
from sqlalchemy import create_engine, Column, Integer, String, DateTime, DECIMAL
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base

DeclarativeBase = declarative_base()
from sqlalchemy import create_engine


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    engine = create_engine("postgresql+psycopg2://user:password@hostname/database_name")
    return engine


def create_imoveis_table(engine):
    DeclarativeBase.metadata.create_all(engine)


class Imoveis(DeclarativeBase):
    """Sqlalchemy imoveis model"""

    __tablename__ = "imoveis"

    code = Column("code", Integer, primary_key=True)
    price = Column("price", DECIMAL(18, 3))
    title = Column("title", String(200), nullable=True)
    url = Column("url", String(200), nullable=True)
    date = Column("date", DateTime, nullable=True)
    regiao = Column("regiao", String(100), nullable=True)

